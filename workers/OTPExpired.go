package workers

import (
	"coreapi/libs/db"
	"fmt"
	"time"

	"gitlab.com/serv4biz/gfp/logs"
)

func OTPExpired() error {
	// Connect to database
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		return err
	}
	defer dbConn.Close()

	_, err = dbConn.DeleteRow("public.phone_otp", fmt.Sprint("(int_stamp + 300) < ", time.Now().UTC().Unix()))
	if logs.Error(err, true) {
		return err
	}

	_, err = dbConn.DeleteRow("public.email_otp", fmt.Sprint("(int_stamp + 300) < ", time.Now().UTC().Unix()))
	if logs.Error(err, true) {
		return err
	}

	_, err = dbConn.DeleteRow("public.telegram_otp", fmt.Sprint("(int_stamp + 300) < ", time.Now().UTC().Unix()))
	if logs.Error(err, true) {
		return err
	}

	_, err = dbConn.DeleteRow("public.secure_otp", fmt.Sprint("(int_stamp + 300) < ", time.Now().UTC().Unix()))
	if logs.Error(err, true) {
		return err
	}

	return nil
}
