package workers

import (
	"coreapi/libs/db"
	"fmt"
	"time"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func ClearSession() error {
	// Connect to database
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		return err
	}
	defer dbConn.Close()

	jsaList, err := dbConn.SelectRow("public.session", "*", fmt.Sprint("(", time.Now().UTC().Unix(), " - int_stamp) >= ", letsgo.MaxSession), "int_stamp desc", 0, -1)
	if logs.Error(err, true) {
		return err
	}

	err = jsaList.EachObject(func(index int, value *jsons.Object) error {
		_, err := dbConn.DeleteRow("public.session", "txt_uid = "+sqlutil.Quote(value.String("txt_uid")))
		if logs.Error(err, true) {
			return err
		}

		if value.String("ref_geoip_txt_uid") != "#" {
			_, err = dbConn.DeleteRow("public.geoip", "txt_uid = "+sqlutil.Quote(value.String("ref_geoip_txt_uid")))
			if logs.Error(err, true) {
				return err
			}
		}
		return nil
	})

	return err
}
