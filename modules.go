package main

import (
	modules_app "coreapi/modules/app"
	modules_email "coreapi/modules/email"
	modules_email_security "coreapi/modules/email/security"
	modules_email_user "coreapi/modules/email/user"
	"gitlab.com/serv4biz/letsgo"
)

func LetsGoModule() {
	letsgo.AddModuleFunc("app/appname", modules_app.AppName)
	letsgo.AddModuleFunc("app/logo", modules_app.Logo)
	letsgo.AddModuleFunc("app/website", modules_app.Website)
	letsgo.AddModuleFunc("email/footer", modules_email.Footer)
	letsgo.AddModuleFunc("email/header", modules_email.Header)
	letsgo.AddModuleFunc("email/style", modules_email.Style)
	letsgo.AddModuleFunc("email/security/otp", modules_email_security.OTP)
	letsgo.AddModuleFunc("email/user/passwd_reset", modules_email_user.Passwd_Reset)

}
