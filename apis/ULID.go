package apis

import (
	"net/http"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

// Generate ULID
func ULID(c *letsgo.Context) error {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	myULID, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_ulid", myULID.String())

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
