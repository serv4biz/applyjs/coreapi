package apis

import (
	"net/http"

	"coreapi/libs/app"
	"coreapi/libs/db"

	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Insert(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtTable, ok := jsoBody.GetString("txt_table")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_table")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoData, ok := jsoBody.GetObject("jso_data")
	if !ok {
		jsoResult.PutString("txt_msg", "require jso_data")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	_, err = dbConn.InsertRow(txtTable, jsoData)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
