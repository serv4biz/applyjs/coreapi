package storage

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Insert(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID, ok := jsoBody.GetString("txt_storage_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_storage_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtName, ok := jsoBody.GetString("txt_name")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_name")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtExt, ok := jsoBody.GetString("txt_ext")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_ext")
		return c.JSON(http.StatusOK, jsoResult)
	}

	intExpire, ok := jsoBody.GetInt("int_expire")
	if !ok {
		jsoResult.PutString("txt_msg", "require int_expire")
		return c.JSON(http.StatusOK, jsoResult)
	}

	intSize, ok := jsoBody.GetInt("int_size")
	if !ok {
		jsoResult.PutString("txt_msg", "require int_size")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoItem := db.NewRow(txtUID)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem.PutString("txt_name", txtName)
	jsoItem.PutString("txt_ext", txtExt)
	jsoItem.PutInt("int_expire", intExpire)
	jsoItem.PutInt("int_size", intSize)

	_, err = dbConn.InsertRow("public.storage", jsoItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
