package contact

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func Add(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtName, ok := jsoBody.GetString("txt_name")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_name")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEmail, ok := jsoBody.GetString("txt_email")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_email")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtPhone, ok := jsoBody.GetString("txt_phone")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_phone")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	myULID, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoItem := db.NewRow(myULID.String())
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem.PutString("txt_name", txtName)
	jsoItem.PutString("txt_email", txtEmail)
	jsoItem.PutString("txt_phone", txtPhone)

	_, err = dbConn.InsertRow("public.contact", jsoItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
