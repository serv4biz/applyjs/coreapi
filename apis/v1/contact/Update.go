package contact

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Update(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID, ok := jsoBody.GetString("txt_contact_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_contact_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoItem := jsons.ObjectNew(0)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtName, ok := jsoBody.GetString("txt_name")
	if ok {
		jsoItem.PutString("txt_name", txtName)
	}

	txtEmail, ok := jsoBody.GetString("txt_email")
	if ok {
		jsoItem.PutString("txt_email", txtEmail)
	}

	txtPhone, ok := jsoBody.GetString("txt_phone")
	if ok {
		jsoItem.PutString("txt_phone", txtPhone)
	}

	_, err = dbConn.UpdateRow("public.contact", jsoItem, "txt_uid = "+sqlutil.Quote(txtUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
