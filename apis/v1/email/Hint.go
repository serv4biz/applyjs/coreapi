package email

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Hint(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()
	// End of connect database

	// Get post value from client
	txtEmailUID, ok := jsoBody.GetString("txt_email_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_email_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	jsoEmail, err := dbConn.FetchRow("public.email", "*", "txt_uid = "+sqlutil.Quote(txtEmailUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtEmail := jsoEmail.String("txt_address")

	txtEmailHint := ""
	intMask := len(txtEmail) / 3
	for i, ch := range txtEmail {
		if i < intMask || i >= (intMask*2) {
			txtEmailHint += string(ch)
		} else {
			txtEmailHint += "*"
		}
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_hint", txtEmailHint)

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
