package email

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"fmt"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func OTP_Check(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()
	// End of connect database

	// Get post value from client
	txtAddress, ok := jsoBody.GetString("txt_address")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_address")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEmailUID, ok := jsoBody.GetString("txt_email_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_email_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtRefCode, ok := jsoBody.GetString("txt_ref_code")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_ref_code")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtOTPCode, ok := jsoBody.GetString("txt_otp_code")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_otp_code")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	if jsoBody.Check("txt_email_uid") && txtEmailUID != "" {
		jsoEmail, err := dbTx.FetchRow("public.email", "*", "txt_uid = "+sqlutil.Quote(txtEmailUID))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtAddress = jsoEmail.String("txt_address")
	}

	// Process
	jsoItem, err := dbTx.FetchRow("public.email_otp", "*", fmt.Sprint("txt_address = ", sqlutil.Quote(txtAddress), " and txt_ref_code = ", sqlutil.Quote(txtRefCode), " and txt_otp_code = ", sqlutil.Quote(txtOTPCode)))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	_, err = dbTx.DeleteRow("public.email_otp", "txt_uid = "+sqlutil.Quote(jsoItem.String("txt_uid")))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtFind := "txt_address = " + sqlutil.Quote(txtAddress)
	blnOk, err := dbTx.ExistRow("public.email", txtFind)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID := ""
	if blnOk {
		rowEmailItem, err := dbTx.FetchRow("public.email", "*", txtFind)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtUID = rowEmailItem.String("txt_uid")
	} else {
		myulid, err := ulids.New()
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtUID = myulid.String()
		jsoEmailItem := db.NewRow(txtUID)
		jsoEmailItem.PutString("txt_address", txtAddress)
		_, err = dbTx.InsertRow("public.email", jsoEmailItem)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	// Secure Token
	myulid, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	rowTokenItem := db.NewRow(myulid.String())
	rowTokenItem.PutString("ref_phone_txt_uid", "#")
	rowTokenItem.PutString("ref_email_txt_uid", txtUID)
	rowTokenItem.PutString("ref_telegram_txt_uid", "#")
	_, err = dbTx.InsertRow("public.secure_otp", rowTokenItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of Secure Token
	// End of process

	// Commit to Database
	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of commit

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_uid", txtUID)
	jsoData.PutString("txt_token", myulid.String())

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
