package email

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"coreapi/libs/sendgrid"
	"fmt"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func OTP_Send(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()
	// End of connect database

	// Get post value from client
	txtAddress, ok := jsoBody.GetString("txt_address")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_address")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEmailUID, ok := jsoBody.GetString("txt_email_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_email_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	if jsoBody.Check("txt_email_uid") && txtEmailUID != "" {
		jsoEmail, err := dbTx.FetchRow("public.email", "*", "txt_uid = "+sqlutil.Quote(txtEmailUID))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtAddress = jsoEmail.String("txt_address")
	}

	// Process
	myulid, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtRefCode := rands.Password(6, 2)
	txtOTPCode := rands.Password(6, 0)
	rowItem := db.NewRow(myulid.String())
	rowItem.PutString("txt_address", txtAddress)
	rowItem.PutString("txt_ref_code", txtRefCode)
	rowItem.PutString("txt_otp_code", txtOTPCode)

	_, err = dbTx.InsertRow("public.email_otp", rowItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of process

	// Commit to Database
	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of commit

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("referral", txtRefCode)
	jsoParams.PutString("code", txtOTPCode)
	for i, ch := range txtOTPCode {
		jsoParams.PutString(fmt.Sprint("ch", i+1), string(ch))
	}
	_, err = sendgrid.Send(c, txtAddress, txtAddress, fmt.Sprint("[-Security OTP is-] ", txtOTPCode, " ( ", txtRefCode, " )"), "email/security/otp", jsoParams)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_ref_code", txtRefCode)

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
