package session

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Put_Data(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtSessionUID, ok := jsoBody.GetString("txt_session_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_session_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoSessData, ok := jsoBody.GetObject("jso_data")
	if !ok {
		jsoResult.PutString("txt_msg", "require jso_data")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtFind := "txt_uid = " + sqlutil.Quote(txtSessionUID)
	blnOk, err := dbConn.ExistRow("public.session", txtFind)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem := db.NewRow(txtSessionUID)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem.PutObject("jso_data", jsoSessData)

	if blnOk {
		jsoItem.Remove("txt_uid")
		jsoItem.Remove("int_stamp")
		_, err = dbConn.UpdateRow("public.session", jsoItem, txtFind)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	} else {
		jsoItem.PutString("ref_user_txt_uid", "#")
		jsoItem.PutString("ref_geoip_txt_uid", "#")
		_, err = dbConn.InsertRow("public.session", jsoItem)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
