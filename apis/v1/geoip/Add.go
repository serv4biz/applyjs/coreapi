package geoip

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func Add(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtDevice, ok := jsoBody.GetString("txt_device")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_device")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtIPAddress, ok := jsoBody.GetString("txt_ip_address")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_ip_address")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtCountryUID, ok := jsoBody.GetString("txt_country_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_country_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	myULID, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoItem := db.NewRow(myULID.String())
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem.PutString("txt_device", txtDevice)
	jsoItem.PutString("txt_ip_address", txtIPAddress)

	blnOk, err := dbConn.ExistRow("country", "txt_uid = "+sqlutil.Quote(txtCountryUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	if blnOk {
		jsoItem.PutString("ref_country_txt_uid", txtCountryUID)
	} else {
		jsoItem.PutString("ref_country_txt_uid", "#")
	}

	_, err = dbConn.InsertRow("public.geoip", jsoItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
