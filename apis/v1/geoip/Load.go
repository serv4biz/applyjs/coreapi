package geoip

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Load(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID, ok := jsoBody.GetString("txt_geoip_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_geoip_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtDevice, deviceOk := jsoBody.GetString("txt_device")
	txtIPAddress, ipOk := jsoBody.GetString("txt_ip_address")
	txtCountryUID, countryOk := jsoBody.GetString("txt_country_uid")

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtFind := "txt_uid = " + sqlutil.Quote(txtUID)
	blnOk, err := dbConn.ExistRow("public.geoip", txtFind)

	var jsoItem *jsons.Object = nil
	if blnOk {
		jsoItem, err = dbConn.FetchRow("public.geoip", "*", txtFind)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	} else {
		jsoItem = db.NewRow(txtUID)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		if !deviceOk {
			txtDevice = ""
		}
		jsoItem.PutString("txt_device", txtDevice)

		if !ipOk {
			txtIPAddress = ""
		}
		jsoItem.PutString("txt_ip_address", txtIPAddress)

		if !countryOk {
			txtCountryUID = "#"
		}
		blnOk, err := dbConn.ExistRow("country", "txt_uid = "+sqlutil.Quote(txtCountryUID))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		if blnOk {
			jsoItem.PutString("ref_country_txt_uid", txtCountryUID)
		} else {
			jsoItem.PutString("ref_country_txt_uid", "#")
		}

		_, err = dbConn.InsertRow("public.geoip", jsoItem)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
