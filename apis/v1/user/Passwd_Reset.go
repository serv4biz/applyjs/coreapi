package user

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"coreapi/libs/sendgrid"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/hash"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/letsgo"
)

func Passwd_Reset(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUsername, ok := jsoBody.GetString("txt_username")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_username")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtUsername = strings.TrimSpace(strings.ToUpper(txtUsername))

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoUserItem, err := dbConn.FetchRow("public.user", "*", "UPPER(txt_username) = "+sqlutil.Quote(txtUsername))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoEmailItem, err := dbConn.FetchRow("email", "*", "txt_uid = "+sqlutil.Quote(jsoUserItem.String("ref_email_txt_uid")))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtNewPass := rands.Password(8, rands.PASSWORD_NUMBER, rands.PASSWORD_LOWERCASE, rands.PASSWORD_UPPERCASE)
	txtEncodePass := hash.SHA256([]byte(txtNewPass))

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_password", txtEncodePass)
	_, err = dbConn.UpdateRow("public.user", jsoData, "txt_uid = "+sqlutil.Quote(jsoUserItem.String("txt_uid")))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEmail := jsoEmailItem.String("txt_address")

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("username", txtUsername)
	jsoParams.PutString("password", txtNewPass)
	_, err = sendgrid.Send(c, txtEmail, txtEmail, "[-Password Reset-]", "email/user/passwd_reset", jsoParams)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
