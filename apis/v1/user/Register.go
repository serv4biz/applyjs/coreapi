package user

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"
	"regexp"
	"strings"
	"unicode/utf8"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/hash"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func Register(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUsername, ok := jsoBody.GetString("txt_username")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_username")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtUsername = strings.TrimSpace(strings.ToUpper(txtUsername))

	txtPassword, ok := jsoBody.GetString("txt_password")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_password")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtPassword = strings.TrimSpace(txtPassword)

	txtCountryUID, ok := jsoBody.GetString("txt_country_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_country_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtCountryUID = strings.TrimSpace(txtCountryUID)

	txtEmailUID, ok := jsoBody.GetString("txt_email_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_email_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtEmailUID = strings.TrimSpace(txtEmailUID)

	txtPhoneUID, ok := jsoBody.GetString("txt_phone_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_phone_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtPhoneUID = strings.TrimSpace(txtPhoneUID)

	txtTelegramUID, ok := jsoBody.GetString("txt_telegram_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_telegram_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtTelegramUID = strings.TrimSpace(txtTelegramUID)

	// check is validate data
	if txtUsername == "" {
		jsoResult.PutString("txt_msg", "Do not empty username")
		return c.JSON(http.StatusOK, jsoResult)
	}

	if txtPassword == "" {
		jsoResult.PutString("txt_msg", "Do not empty password")
		return c.JSON(http.StatusOK, jsoResult)
	}

	reg, err := regexp.Compile("^[a-zA-Z0-9]+$")
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	if !reg.MatchString(txtUsername) {
		jsoResult.PutString("txt_msg", "The username must use letters a-z and A-Z and 0-9 only")
		return c.JSON(http.StatusOK, jsoResult)
	}

	if utf8.RuneCountInString(txtUsername) <= 5 {
		jsoResult.PutString("txt_msg", "The username must be at least 6 characters")
		return c.JSON(http.StatusOK, jsoResult)
	}

	if utf8.RuneCountInString(txtPassword) <= 5 {
		jsoResult.PutString("txt_msg", "The password must be at least 6 characters")
		return c.JSON(http.StatusOK, jsoResult)
	}

	if strings.HasPrefix(txtUsername, "ADMIN") || strings.HasSuffix(txtUsername, "ADMIN") {
		jsoResult.PutString("txt_msg", "The name contains a prohibited word")
		return c.JSON(http.StatusOK, jsoResult)
	}

	// database
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()

	// Check duplicate username
	txtFind := "txt_username = " + sqlutil.Quote(txtUsername)
	blnExist, err := dbTx.ExistRow("public.user", txtFind)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	if blnExist {
		jsoResult.PutString("txt_msg", "The username is already in use")
		return c.JSON(http.StatusOK, jsoResult)
	}

	myulid, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEncodePass := hash.SHA256([]byte(txtPassword))

	jsoNewUser := db.NewRow(myulid.String())
	jsoNewUser.PutString("txt_username", txtUsername)
	jsoNewUser.PutString("txt_password", txtEncodePass)
	jsoNewUser.PutString("ref_group_txt_uid", "USER")
	jsoNewUser.PutString("ref_country_txt_uid", txtCountryUID)
	jsoNewUser.PutString("ref_email_txt_uid", txtEmailUID)
	jsoNewUser.PutString("ref_phone_txt_uid", txtPhoneUID)
	jsoNewUser.PutString("ref_telegram_txt_uid", txtTelegramUID)
	jsoNewUser.PutBool("bln_active", true)

	_, err = dbTx.InsertRow("public.user", jsoNewUser)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutObject("jso_data", jsoNewUser)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
