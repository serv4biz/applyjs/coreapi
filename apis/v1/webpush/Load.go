package webpush

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Load(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID, ok := jsoBody.GetString("txt_webpush_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_webpush_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtEndPoint, ok := jsoBody.GetString("txt_endpoint")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_endpoint")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtAuth, ok := jsoBody.GetString("txt_auth")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_auth")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtP256DH, ok := jsoBody.GetString("txt_p256dh")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_p256dh")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtGeoIpUID, ok := jsoBody.GetString("txt_geoip_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_geoip_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtFind := "txt_uid = " + sqlutil.Quote(txtUID)
	blnOk, err := dbConn.ExistRow("public.webpush", txtFind)

	var jsoItem *jsons.Object = nil
	if blnOk {
		jsoItem, err = dbConn.FetchRow("public.webpush", "*", txtFind)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	} else {
		jsoItem = db.NewRow(txtUID)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}

		jsoItem.PutString("txt_endpoint", txtEndPoint)
		jsoItem.PutString("txt_auth", txtAuth)
		jsoItem.PutString("txt_p256dh", txtP256DH)
		jsoItem.PutString("ref_geoip_txt_uid", txtGeoIpUID)

		_, err = dbConn.InsertRow("public.webpush", jsoItem)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
