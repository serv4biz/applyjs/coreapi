package telegram

import (
	"coreapi/libs/db"
	"coreapi/libs/telegram"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/dbs"
	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func MessageProfile(dbConn dbs.DX, jsoUserInfo *jsons.Object) error {
	userId := fmt.Sprint(jsoUserInfo.Int("id"))

	blnOk, err := dbConn.ExistRow("public.telegram", "txt_uid = "+sqlutil.Quote(userId))
	if err != nil {
		return err
	}

	jsoData := db.NewRow(userId)
	jsoData.PutString("txt_first_name", jsoUserInfo.String("first_name"))
	jsoData.PutString("txt_last_name", jsoUserInfo.String("last_name"))
	jsoData.PutString("txt_username", jsoUserInfo.String("username"))
	jsoData.PutString("txt_language", jsoUserInfo.String("language_code"))

	txtPicture := ""
	jsoPhotos, err := telegram.GetUserProfilePhotos(userId)
	if err != nil {
		return err
	}
	if jsoPhotos.Bool("ok") {
		if jsoPhotos.Object("result").Int("total_count") > 0 {
			jsaPhoto := jsoPhotos.Object("result").Array("photos").GetArray(0)
			fileId := ""
			fileSize := 0
			jsaPhoto.EachObject(func(index int, value *jsons.Object) error {
				if value.Int("file_size") > fileSize {
					fileSize = value.Int("file_size")
					fileId = value.String("file_id")
				}
				return nil
			})

			jsoFile, err := telegram.GetFile(fileId)
			if err != nil {
				return err
			}
			if jsoFile.Bool("ok") {
				jsoConfig, err := letsgo.GetConfig("telegram")
				if err != nil {
					return err
				}

				resp, err := http.Get("https://api.telegram.org/file/bot" + jsoConfig.String("txt_access_token") + "/" + jsoFile.Object("result").String("file_path"))
				if err == nil {
					defer resp.Body.Close()

					buffs, err := io.ReadAll(resp.Body)
					if err != nil {
						return err
					}

					appDir, err := files.GetAppDir()
					if err != nil {
						return err
					}

					ext := ".jpg"
					if resp.Header.Get("Content-Type") == "image/png" {
						ext = ".png"
					}

					pathDir := "/platforms/telegram/img"
					files.MakeDir(fmt.Sprint(appDir, pathDir), fs.ModePerm)

					pathFile := fmt.Sprint(pathDir, "/", userId, ext)
					_, err = files.WriteFile(fmt.Sprint(appDir, pathFile), buffs, fs.ModePerm)
					if err != nil {
						return err
					}
					txtPicture = pathFile
				}
			}
		}
	}
	jsoData.PutString("txt_picture", txtPicture)

	if blnOk {
		jsoData.Remove("txt_uid")
		jsoData.Remove("int_stamp")
		_, err := dbConn.UpdateRow("public.telegram", jsoData, "txt_uid = "+sqlutil.Quote(userId))
		if err != nil {
			return err
		}
	} else {
		_, err := dbConn.InsertRow("public.telegram", jsoData)
		if err != nil {
			return err
		}
	}
	return nil
}

func MessageOTP(dbConn dbs.DX, userId string) error {
	myulid, err := ulids.New()
	if err != nil {
		return err
	}
	_, err = dbConn.DeleteRow("public.telegram_otp", "ref_telegram_txt_uid = "+sqlutil.Quote(userId))
	if err != nil {
		return err
	}

	txtRefCode := rands.Password(6, 2)
	txtOTPCode := rands.Password(6, 0)

	for {
		blnOk, err := dbConn.ExistRow("public.telegram_otp", "txt_otp_code = "+sqlutil.Quote(txtOTPCode))
		if err != nil {
			return err
		}

		if blnOk {
			txtOTPCode = rands.Password(6, 0)
		} else {
			break
		}
	}

	rowItem := db.NewRow(myulid.String())
	rowItem.PutString("ref_telegram_txt_uid", userId)
	rowItem.PutString("txt_ref_code", txtRefCode)
	rowItem.PutString("txt_otp_code", txtOTPCode)

	_, err = dbConn.InsertRow("public.telegram_otp", rowItem)
	if err != nil {
		return err
	}
	txtMessage := "Security OTP is " + txtOTPCode + "\nOTP will expire in 5 minutes\n( " + txtRefCode + " )"

	_, err = telegram.SendMessage(userId, txtMessage)
	if err != nil {
		return err
	}
	return nil
}

func MessageConnect(dbConn dbs.DX, userId string) error {
	myulid, err := ulids.New()
	if err != nil {
		return err
	}
	_, err = dbConn.DeleteRow("public.telegram_connect", "ref_telegram_txt_uid = "+sqlutil.Quote(userId))
	if err != nil {
		return err
	}

	rowItem := db.NewRow(myulid.String())
	rowItem.PutString("ref_telegram_txt_uid", userId)

	_, err = dbConn.InsertRow("public.telegram_connect", rowItem)
	if err != nil {
		return err
	}

	_, err = telegram.SendMessage(userId, "The new connect code is")
	if err != nil {
		return err
	}

	_, err = telegram.SendMessage(userId, myulid.String())
	if err != nil {
		return err
	}
	return nil
}

func Message(c *letsgo.Context) error {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	jsoBody, err := c.BodyObject()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()

	jsoMessage := jsoBody.Object("message")
	jsoUserInfo := jsoMessage.Object("from")
	txtMessage := jsoMessage.String("text")

	err = MessageProfile(dbTx, jsoUserInfo)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	if strings.EqualFold(txtMessage, "/connect") {
		err = MessageConnect(dbTx, fmt.Sprint(jsoUserInfo.Int("id")))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
