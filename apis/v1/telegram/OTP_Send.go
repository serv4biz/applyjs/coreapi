package telegram

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"coreapi/libs/telegram"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func OTP_Send(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()
	// End of connect database

	// Get post value from client
	txtUserUID, ok := jsoBody.GetString("txt_user_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_user_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	// Process
	jsoUser, err := dbTx.FetchRow("public.user", "*", "txt_uid = "+sqlutil.Quote(txtUserUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtTelegramUID := jsoUser.String("ref_telegram_txt_uid")

	myulid, err := ulids.New()
	if err != nil {
		return err
	}
	_, err = dbTx.DeleteRow("public.telegram_otp", "ref_telegram_txt_uid = "+sqlutil.Quote(txtTelegramUID))
	if err != nil {
		return err
	}

	txtRefCode := rands.Password(6, 2)
	txtOTPCode := rands.Password(6, 0)

	for {
		blnOk, err := dbTx.ExistRow("public.telegram_otp", "txt_otp_code = "+sqlutil.Quote(txtOTPCode))
		if err != nil {
			return err
		}

		if blnOk {
			txtOTPCode = rands.Password(6, 0)
		} else {
			break
		}
	}

	rowItem := db.NewRow(myulid.String())
	rowItem.PutString("ref_telegram_txt_uid", txtTelegramUID)
	rowItem.PutString("txt_ref_code", txtRefCode)
	rowItem.PutString("txt_otp_code", txtOTPCode)

	_, err = dbTx.InsertRow("public.telegram_otp", rowItem)
	if err != nil {
		return err
	}
	txtMessage := "Security OTP is " + txtOTPCode + "\nOTP will expire in 5 minutes\n( " + txtRefCode + " )"

	_, err = telegram.SendMessage(txtTelegramUID, txtMessage)
	if err != nil {
		return err
	}
	// End of process

	// Commit to Database
	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of commit

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_ref_code", txtRefCode)

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
