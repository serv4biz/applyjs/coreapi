package telegram

import (
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/letsgo"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// Stream is stream data
func Stream(c *letsgo.Context) error {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	jsoConfig, err := letsgo.GetConfig("telegram")
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	letsgo.UseGet(c)
	filePath := c.GET.String("path")

	spls := strings.Split(filePath, ".")
	txtExtName := spls[len(spls)-1]

	resp, err := http.Get("https://api.telegram.org/file/bot" + jsoConfig.String("txt_access_token") + "/" + filePath)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	buffer, err := io.ReadAll(resp.Body)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoMimetype, err := letsgo.GetConfig("mimetype")
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	if txtExtName == "" {
		txtExtName = "jpg"
	}

	if !jsoMimetype.Check(txtExtName) {
		jsoResult.PutString("txt_msg", "Not found ext in mimetype")
		return c.JSON(http.StatusOK, jsoResult)
	}

	c.Response.Header().Del("Content-Type")
	c.Response.Header().Set("Content-Type", jsoMimetype.String(txtExtName))
	c.Response.Write(buffer)
	return nil
}
