package phone

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func Add(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtCountryUID, ok := jsoBody.GetString("txt_country_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_country_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtNumber, ok := jsoBody.GetString("txt_number")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_number")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtNumber = strings.TrimSpace(txtNumber)

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	blnOk, err := dbConn.ExistRow("public.phone", "ref_country_txt_uid = "+sqlutil.Quote(txtCountryUID)+" and txt_number = "+sqlutil.Quote(txtNumber))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	if blnOk {
		jsoResult.PutString("txt_msg", "phone number already exists")
		return c.JSON(http.StatusOK, jsoResult)
	}

	myULID, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoItem := db.NewRow(myULID.String())
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	jsoItem.PutString("ref_country_txt_uid", txtCountryUID)
	jsoItem.PutString("txt_number", txtNumber)

	_, err = dbConn.InsertRow("public.phone", jsoItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	jsoResult.PutObject("jso_data", jsoItem)
	return c.JSON(http.StatusOK, jsoResult)
}
