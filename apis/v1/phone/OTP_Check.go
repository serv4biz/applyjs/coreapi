package phone

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func OTP_Check(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()
	// End of connect database

	// Get post value from client
	txtPhoneUID, ok := jsoBody.GetString("txt_phone_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_phone_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtNumber, ok := jsoBody.GetString("txt_number")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_number")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtNumber = strings.TrimSpace(txtNumber)

	txtCountryUID, ok := jsoBody.GetString("txt_country_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_country_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtRefCode, ok := jsoBody.GetString("txt_ref_code")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_ref_code")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtOTPCode, ok := jsoBody.GetString("txt_otp_code")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_otp_code")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	if jsoBody.Check("txt_phone_uid") && txtPhoneUID != "" {
		jsoPhone, err := dbConn.FetchRow("phone", "*", "txt_uid = "+sqlutil.Quote(txtPhoneUID))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtNumber = jsoPhone.String("txt_number")
		txtCountryUID = jsoPhone.String("ref_country_txt_uid")
	}

	// Process
	rowItem, err := dbTx.FetchRow("phone_otp", "*", fmt.Sprint("txt_number = ", sqlutil.Quote(txtNumber), " and txt_ref_code = ", sqlutil.Quote(txtRefCode), " and txt_otp_code = ", sqlutil.Quote(txtOTPCode), " and ref_country_txt_uid = ", sqlutil.Quote(txtCountryUID)))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	_, err = dbTx.DeleteRow("phone_otp", "txt_uid = "+sqlutil.Quote(rowItem.String("txt_uid")))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtFind := "txt_number = " + sqlutil.Quote(txtNumber) + " and ref_country_txt_uid = " + sqlutil.Quote(txtCountryUID)
	blnOk, err := dbTx.ExistRow("phone", txtFind)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtUID := ""
	if blnOk {
		rowPhoneItem, err := dbTx.FetchRow("phone", "*", txtFind)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtUID = rowPhoneItem.String("txt_uid")

		_, err = dbTx.UpdateRow("phone", rowPhoneItem, "txt_uid = "+sqlutil.Quote(rowPhoneItem.String("txt_uid")))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	} else {
		myulid, err := ulids.New()
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtUID = myulid.String()
		rowPhoneItem := db.NewRow(txtUID)
		rowPhoneItem.PutString("txt_number", txtNumber)
		rowPhoneItem.PutString("ref_country_txt_uid", txtCountryUID)
		_, err = dbTx.InsertRow("phone", rowPhoneItem)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}

	// Secure Token
	myulid, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	rowTokenItem := db.NewRow(myulid.String())
	rowTokenItem.PutString("ref_phone_txt_uid", txtUID)
	rowTokenItem.PutString("ref_email_txt_uid", "#")
	rowTokenItem.PutString("ref_telegram_txt_uid", "#")
	_, err = dbTx.InsertRow("secure_otp", rowTokenItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of Secure Token

	// End of process

	// Commit to Database
	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of commit

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_uid", txtUID)
	jsoData.PutString("txt_token", myulid.String())

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
