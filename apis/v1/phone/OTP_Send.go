package phone

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"coreapi/libs/smsgateway"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/gfp/ulids"
	"gitlab.com/serv4biz/letsgo"
)

func OTP_Send(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()

	dbTx, err := dbConn.Begin()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbTx.Rollback()
	// End of connect database

	// Get post value from client
	txtPhoneUID, ok := jsoBody.GetString("txt_phone_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_phone_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtNumber, ok := jsoBody.GetString("txt_number")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_number")
		return c.JSON(http.StatusOK, jsoResult)
	}
	txtNumber = strings.TrimSpace(txtNumber)

	txtCountryUID, ok := jsoBody.GetString("txt_country_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_country_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtType, ok := jsoBody.GetString("txt_type")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_type")
		return c.JSON(http.StatusOK, jsoResult)
	}
	if txtType == "" {
		txtType = "SMS"
	}
	// End of get post value

	if jsoBody.Check("txt_phone_uid") && txtPhoneUID != "" {
		jsoPhone, err := dbConn.FetchRow("phone", "*", "txt_uid = "+sqlutil.Quote(txtPhoneUID))
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
		txtNumber = jsoPhone.String("txt_number")
		txtCountryUID = jsoPhone.String("ref_country_txt_uid")
	}

	// Process
	myulid, err := ulids.New()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtRefCode := rands.Password(6, 2)
	txtOTPCode := rands.Password(6, 0)

	rowItem := db.NewRow(myulid.String())
	rowItem.PutString("ref_country_txt_uid", txtCountryUID)
	rowItem.PutString("txt_number", txtNumber)
	rowItem.PutString("txt_ref_code", txtRefCode)
	rowItem.PutString("txt_otp_code", txtOTPCode)

	_, err = dbTx.InsertRow("phone_otp", rowItem)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoCountry, err := dbTx.FetchRow("country", "*", "txt_uid = "+sqlutil.Quote(txtCountryUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	if txtType == "SMS" {
		txtMessage := "Security OTP is " + txtOTPCode + "\nOTP will expire in 5 minutes\n( " + txtRefCode + " )"
		err = smsgateway.Send(fmt.Sprint("+", jsoCountry.String("txt_call"), txtNumber), txtMessage)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	} else if txtType == "VOICE" {
		txtMessage := "OTP is "
		if txtCountryUID == "TH" {
			txtMessage = "OTP ของท่านคือ"
		}
		for _, char := range txtOTPCode {
			txtMessage += fmt.Sprint(" ... ... ", string(char))
		}

		err = smsgateway.Voice(fmt.Sprint("+", jsoCountry.String("txt_call"), txtNumber), txtMessage)
		if logs.Error(err, true) {
			jsoResult.PutString("txt_msg", logs.Message(err))
			return c.JSON(http.StatusOK, jsoResult)
		}
	}
	// End of process

	// Commit to Database
	err = dbTx.Commit()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of commit

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_ref_code", txtRefCode)

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
