package phone

import (
	"coreapi/libs/app"
	"coreapi/libs/db"
	"net/http"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Hint(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	// Connect to database with transaction
	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}
	defer dbConn.Close()
	// End of connect database

	// Get post value from client
	txtPhoneUID, ok := jsoBody.GetString("txt_phone_uid")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_phone_uid")
		return c.JSON(http.StatusOK, jsoResult)
	}
	// End of get post value

	jsoPhone, err := dbConn.FetchRow("phone", "*", "txt_uid = "+sqlutil.Quote(txtPhoneUID))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoCountry, err := dbConn.FetchRow("country", "*", "txt_uid = "+sqlutil.Quote(jsoPhone.String("ref_country_txt_uid")))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtPhoneNumber := jsoPhone.String("txt_number")

	txtPhoneHint := ""
	intMask := len(txtPhoneNumber) / 3
	for i, ch := range txtPhoneNumber {
		if i < intMask || i >= (intMask*2) {
			txtPhoneHint += string(ch)
		} else {
			txtPhoneHint += "*"
		}
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_hint", "+"+jsoCountry.String("txt_call")+txtPhoneHint)

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
