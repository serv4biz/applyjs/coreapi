package apis

import (
	"net/http"

	"coreapi/libs/app"
	"coreapi/libs/db"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Delete(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	txtTable, ok := jsoBody.GetString("txt_table")
	if !ok {
		jsoResult.PutString("txt_msg", "require txt_table")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaFind, ok := jsoBody.GetArray("jsa_find")
	if !ok {
		jsoResult.PutString("txt_msg", "require jsa_find")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	_, err = dbConn.DeleteRow(txtTable, sqlutil.MakeWhere(jsaFind))
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
