package apis

import (
	"net/http"

	"coreapi/libs/app"

	"coreapi/libs/db"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func Query(c *letsgo.Context) error {
	jsoResult, jsoBody, err := app.Init(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	err = app.CheckKey(c)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaTable, ok := jsoBody.GetArray("jsa_table")
	if !ok {
		jsoResult.PutString("txt_msg", "require jsa_table")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaColumn, ok := jsoBody.GetArray("jsa_column")
	if !ok {
		jsoResult.PutString("txt_msg", "require jsa_column")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaFind, ok := jsoBody.GetArray("jsa_find")
	if !ok {
		jsoResult.PutString("txt_msg", "require jsa_find")
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaSort, ok := jsoBody.GetArray("jsa_sort")
	if !ok {
		jsoResult.PutString("txt_msg", "require jsa_sort")
		return c.JSON(http.StatusOK, jsoResult)
	}

	intOffset, ok := jsoBody.GetInt("int_offset")
	if !ok {
		jsoResult.PutString("txt_msg", "require int_offset")
		return c.JSON(http.StatusOK, jsoResult)
	}

	intLimit, ok := jsoBody.GetInt("int_limit")
	if !ok {
		jsoResult.PutString("txt_msg", "require int_limit")
		return c.JSON(http.StatusOK, jsoResult)
	}

	dbConn, err := db.Connect()
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsaList, err := db.Query(dbConn, jsaTable, jsaColumn, jsaFind, jsaSort, intOffset, intLimit)
	if logs.Error(err, true) {
		jsoResult.PutString("txt_msg", logs.Message(err))
		return c.JSON(http.StatusOK, jsoResult)
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutArray("jsa_list", jsaList)
	jsoData.PutInt("int_length", jsaList.Length())

	jsoResult.PutObject("jso_data", jsoData)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
