package webpush

import (
	"coreapi/libs/db"
	"coreapi/libs/webpush/network"
	"errors"

	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func SendNotification(subUID string, title string, jsoOption *jsons.Object) error {
	jsoConfig, err := letsgo.GetConfig("webpush")
	if err != nil {
		return err
	}

	dbConn, err := db.Connect()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	jsoSub, err := dbConn.FetchRow("public.webpush", "*", "txt_uid = "+sqlutil.Quote(subUID))
	if err != nil {
		return err
	}

	jsoData := jsons.ObjectNew(0)
	jsoData.PutString("txt_title", title)
	jsoData.PutObject("jso_option", jsoOption)

	body := jsons.ObjectNew(0)
	body.PutObject("jso_config", jsoConfig)
	body.PutObject("jso_subscribe", jsoSub)
	body.PutObject("jso_data", jsoData)

	jsoReq, err := network.Request("push", body)
	if err != nil {
		return err
	}
	if jsoReq.Int("int_status") == 0 {
		return errors.New(jsoReq.String("txt_msg"))
	}
	return nil
}
