package network

import (
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/ulids"
)

func Send(path string, data *jsons.Object) (*jsons.Object, error) {
	url := strings.ToLower(path)
	method := "POST"

	myulid, err := ulids.New()
	if err != nil {
		return nil, err
	}

	data.PutString("txt_referral", myulid.String())
	buff, err := data.ToString()
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	req, err := http.NewRequest(method, url, strings.NewReader(buff))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	return jsons.ObjectFromByte(body)
}
