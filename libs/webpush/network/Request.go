package network

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Request(path string, data *jsons.Object) (*jsons.Object, error) {
	jsoConfig, err := letsgo.GetConfig("webpush")
	if err != nil {
		return nil, err
	}
	url := jsoConfig.Object("jso_push_node").String("txt_endpoint") + "/" + jsoConfig.Object("jso_push_node").String("txt_version") + "/" + strings.ToLower(path)
	return Send(url, data)
}
