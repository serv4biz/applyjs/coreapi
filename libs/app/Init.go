package app

import (
	"errors"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Init(c *letsgo.Context) (*jsons.Object, *jsons.Object, error) {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 0)

	jsoBody, err := c.BodyObject()
	if err != nil {
		return jsoResult, jsons.ObjectNew(0), err
	}
	refCode, ok := jsoBody.GetString("txt_referral")
	if !ok {
		return jsoResult, jsons.ObjectNew(0), errors.New("require txt_referral")
	}
	jsoResult.PutString("txt_referral", refCode)

	return jsoResult, jsoBody, err
}
