package app

import (
	"errors"

	"gitlab.com/serv4biz/letsgo"
)

func CheckKey(c *letsgo.Context) error {
	key := c.Request.Header.Get("api-key")

	item, ok := MapAPIKey[key]
	if !ok {
		return errors.New("api-key not found")
	}
	if item == nil {
		return errors.New("api-key no permission")
	}
	return nil
}
