package app

import (
	"errors"

	"gitlab.com/serv4biz/letsgo"
)

func GetKey(c *letsgo.Context) (*APIKey, error) {
	key := c.Request.Header.Get("api-key")

	item, ok := MapAPIKey[key]
	if !ok {
		return nil, errors.New("api-key not found")
	}

	return item, nil
}
