package app

import "gitlab.com/serv4biz/gfp/jsons"

type APIKey struct {
	Key string
}

var MapAPIKey map[string]*APIKey = make(map[string]*APIKey)

var JSOInfo *jsons.Object
