package thsms

import (
	"errors"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Send(txtNumber string, txtMessage string) error {
	jsoConfig, errConfig := letsgo.GetConfig("thsms")
	if errConfig != nil {
		return errConfig
	}

	txtURL := "https://thsms.com/api/send-sms"
	method := "POST"

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("sender", jsoConfig.String("txt_sender"))
	jsoParams.PutString("message", txtMessage)
	jsaMSISDN := jsons.ArrayNew(0)
	jsoParams.PutArray("msisdn", jsaMSISDN.PutString(strings.ReplaceAll(txtNumber, "+", "")))

	txtJSON, err := jsoParams.ToString()
	if err != nil {
		return err
	}
	payload := strings.NewReader(txtJSON)

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+jsoConfig.String("txt_apitoken"))
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	jsoRes, err := jsons.ObjectFromString(string(body))
	if err != nil {
		return err
	}

	if !jsoRes.Bool("success") {
		return errors.New(jsoRes.String("errors"))
	}
	return nil
}
