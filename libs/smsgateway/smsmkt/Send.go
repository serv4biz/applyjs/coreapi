package smsmkt

import (
	"errors"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Send(txtNumber string, txtMessage string) error {
	jsoConfig, errConfig := letsgo.GetConfig("smsmkt")
	if errConfig != nil {
		return errConfig
	}
	txtURL := "https://portal-otp.smsmkt.com/api/send-message"
	method := "POST"

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("sender", jsoConfig.String("txt_sender"))
	jsoParams.PutString("message", txtMessage)
	jsoParams.PutString("phone", strings.ReplaceAll(txtNumber, "+", ""))

	txtJSON, err := jsoParams.ToString()
	if err != nil {
		return err
	}
	payload := strings.NewReader(txtJSON)

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("api_key", jsoConfig.String("txt_api_key"))
	req.Header.Add("secret_key", jsoConfig.String("txt_secret_key"))

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	jsoRes, err := jsons.ObjectFromString(string(body))
	if err != nil {
		return err
	}

	if jsoRes.String("code") != "000" {
		return errors.New(jsoRes.String("detail"))
	}
	return nil
}
