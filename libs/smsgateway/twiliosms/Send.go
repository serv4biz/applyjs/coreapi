package twiliosms

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Send(txtNumber string, txtMessage string) error {
	jsoConfig, errConfig := letsgo.GetConfig("twiliosms")
	if errConfig != nil {
		return errConfig
	}
	txtURL := "https://api.twilio.com/2010-04-01/Accounts/" + jsoConfig.String("txt_account_sid") + "/Messages.json"
	method := "POST"

	jsaSender := jsoConfig.Array("jsa_sender")
	intSelect := rand.Intn(jsaSender.Length())
	txtSender := jsaSender.String(intSelect)

	txtToNumber := txtNumber
	if !strings.HasPrefix(txtToNumber, "+") {
		txtToNumber = "+" + txtToNumber
	}

	vals := url.Values{}
	vals.Set("From", txtSender)
	vals.Set("To", txtToNumber)
	vals.Set("Body", txtMessage)
	payload := strings.NewReader(vals.Encode())

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return err
	}
	txtToken := base64.StdEncoding.EncodeToString([]byte(fmt.Sprint(jsoConfig.String("txt_account_sid"), ":", jsoConfig.String("txt_auth_token"))))
	req.Header.Add("Authorization", "Basic "+txtToken)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	jsoRes, err := jsons.ObjectFromString(string(body))
	if err != nil {
		return err
	}

	if res.StatusCode != 201 {
		return errors.New(fmt.Sprint("Code ", jsoRes.Int("code"), " : ", jsoRes.String("message")))
	}
	return nil
}
