package smsgateway

import (
	"coreapi/libs/smsgateway/messagebird"
	"errors"
	"strings"

	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/letsgo"
)

func Voice(txtNumber string, txtMessage string) error {
	jsoConfig, err := letsgo.GetConfig("smsgateway")
	if err != nil {
		return err
	}

	blnFound := false
	jsoVoice := jsoConfig.Object("jso_voice_zone")
	jsoVoice.EachArray(func(key string, jsaType *jsons.Array) error {
		if strings.HasPrefix(txtNumber, key) {
			jsoItem := jsaType.Object(rands.Number(jsaType.Length()))

			switch jsoItem.String("txt_provider") {
			case "messagebird":
				err := messagebird.Voice(txtNumber, txtMessage, jsoItem.String("txt_language"))
				if err != nil {
					return err
				}
				blnFound = true
				return errs.ErrBreak
			}
		}
		return nil
	})

	if blnFound {
		return nil
	}

	// If no zone
	jsaType := jsoConfig.Array("jsa_voice")
	jsoItem := jsaType.Object(rands.Number(jsaType.Length()))

	switch jsoItem.String("txt_provider") {
	case "messagebird":
		err := messagebird.Voice(txtNumber, txtMessage, jsoItem.String("txt_language"))
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("not support gateway")
}
