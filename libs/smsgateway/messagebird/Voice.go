package messagebird

import (
	"errors"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Voice(txtNumber string, txtMessage string, txtLanguage string) error {
	txtLang := txtLanguage
	if txtLanguage == "" {
		txtLang = "en-us"
	}
	jsoConfig, errConfig := letsgo.GetConfig("messagebird")
	if errConfig != nil {
		return errConfig
	}
	txtURL := "https://rest.messagebird.com/voicemessages"
	method := "POST"

	jsaSender := jsoConfig.Array("jsa_voice_sender")
	intSelect := rand.Intn(jsaSender.Length())
	txtSender := jsaSender.String(intSelect)

	txtToNumber := txtNumber
	if !strings.HasPrefix(txtToNumber, "+") {
		txtToNumber = "+" + txtToNumber
	}

	vals := url.Values{}
	vals.Set("originator", txtSender)
	vals.Set("recipients", txtToNumber)
	vals.Set("body", txtMessage)
	vals.Set("language", txtLang)
	vals.Set("voice", "female")
	vals.Set("repeat", "5")
	payload := strings.NewReader(vals.Encode())

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "AccessKey "+jsoConfig.String("txt_access_key"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	jsoRes, err := jsons.ObjectFromString(string(body))
	if err != nil {
		return err
	}

	if res.StatusCode != 201 {
		jsaErrors := jsoRes.Array("errors")
		txtJSON, err := jsaErrors.ToString()
		if err != nil {
			return err
		}
		return errors.New(txtJSON)
	}
	return nil
}
