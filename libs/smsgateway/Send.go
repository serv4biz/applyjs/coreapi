package smsgateway

import (
	"coreapi/libs/smsgateway/sms2pro"
	"coreapi/libs/smsgateway/smsmkt"
	"coreapi/libs/smsgateway/thsms"
	"coreapi/libs/smsgateway/twiliosms"
	"errors"
	"strings"

	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/rands"
	"gitlab.com/serv4biz/letsgo"
)

func Send(txtNumber string, txtMessage string) error {
	jsoConfig, err := letsgo.GetConfig("smsgateway")
	if err != nil {
		return err
	}

	blnFound := false
	jsoSMS := jsoConfig.Object("jso_sms_zone")
	jsoSMS.EachArray(func(key string, jsaType *jsons.Array) error {
		if strings.HasPrefix(txtNumber, key) {
			txtType := jsaType.String(rands.Number(jsaType.Length()))

			switch txtType {
			case "thsms":
				err := thsms.Send(txtNumber, txtMessage)
				if err != nil {
					return err
				}
				blnFound = true
				return errs.ErrBreak
			case "sms2pro":
				err := sms2pro.Send(txtNumber, txtMessage)
				if err != nil {
					return err
				}
				blnFound = true
				return errs.ErrBreak
			case "smsmkt":
				err := smsmkt.Send(txtNumber, txtMessage)
				if err != nil {
					return err
				}
				blnFound = true
				return errs.ErrBreak
			case "twiliosms":
				err := twiliosms.Send(txtNumber, txtMessage)
				if err != nil {
					return err
				}
				blnFound = true
				return errs.ErrBreak
			}
		}
		return nil
	})

	if blnFound {
		return nil
	}

	// If no zone
	jsaType := jsoConfig.Array("jsa_sms")
	txtType := jsaType.String(rands.Number(jsaType.Length()))

	switch txtType {
	case "thsms":
		err := thsms.Send(txtNumber, txtMessage)
		if err != nil {
			return err
		}
		return nil
	case "sms2pro":
		err := sms2pro.Send(txtNumber, txtMessage)
		if err != nil {
			return err
		}
		return nil
	case "smsmkt":
		err := smsmkt.Send(txtNumber, txtMessage)
		if err != nil {
			return err
		}
		return nil
	case "twiliosms":
		err := twiliosms.Send(txtNumber, txtMessage)
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("not support gateway")
}
