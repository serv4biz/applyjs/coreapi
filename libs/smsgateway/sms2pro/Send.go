package sms2pro

import (
	"errors"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Send(txtNumber string, txtMessage string) error {
	jsoConfig, errConfig := letsgo.GetConfig("sms2pro")
	if errConfig != nil {
		return errConfig
	}

	txtURL := "https://client.sms2pro.com/api/v3/sms/send"
	method := "POST"

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("type", "otp")
	jsoParams.PutString("sender_id", jsoConfig.String("txt_sender"))
	jsoParams.PutString("recipient", strings.ReplaceAll(txtNumber, "+", ""))
	jsoParams.PutString("message", txtMessage)

	txtJSON, err := jsoParams.ToString()
	if err != nil {
		return err
	}
	payload := strings.NewReader(txtJSON)

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+jsoConfig.String("txt_apitoken"))
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	jsoRes, err := jsons.ObjectFromString(string(body))
	if err != nil {
		return err
	}

	if jsoRes.String("status") != "success" {
		return errors.New(jsoRes.String("message"))
	}
	return nil
}
