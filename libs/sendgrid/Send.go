package sendgrid

import (
	"github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Send(c *letsgo.Context, toName string, toAddress string, txtSubject string, txtModule string, jsoParams *jsons.Object) (*rest.Response, error) {
	txtContent := c.Module(txtModule, jsoParams)
	subject := c.Translate(txtSubject)

	jsoConfig, errConfig := letsgo.GetConfig("sendgrid")
	if errConfig != nil {
		return nil, errConfig
	}

	fromName := jsoConfig.String("txt_name")
	fromAddress := jsoConfig.String("txt_address")

	from := mail.NewEmail(fromName, fromAddress)
	to := mail.NewEmail(toName, toAddress)
	message := mail.NewSingleEmail(from, subject, to, txtContent, txtContent)
	client := sendgrid.NewSendClient(jsoConfig.String("txt_apikey"))
	return client.Send(message)
}
