package db

import (
	"gitlab.com/serv4biz/gfp/dbs"
	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
)

func Query(conn dbs.DX, tables *jsons.Array, columns *jsons.Array, finds *jsons.Array, sorts *jsons.Array, offset int, limit int) (*jsons.Array, error) {
	sql := sqlutil.MakeQuery(tables, columns, finds, sorts, offset, limit)
	return conn.Query(sql)
}
