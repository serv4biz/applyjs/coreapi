package db

import (
	"gitlab.com/serv4biz/gfp/dbs"
	"gitlab.com/serv4biz/gfp/dbs/sqlutil"
	"gitlab.com/serv4biz/gfp/jsons"
)

func Fetch(conn dbs.DX, tables *jsons.Array, columns *jsons.Array, finds *jsons.Array) (*jsons.Object, error) {
	sql := sqlutil.MakeQuery(tables, columns, finds, jsons.ArrayNew(0), 0, 1)
	return conn.Fetch(sql)
}
