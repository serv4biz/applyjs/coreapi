package db

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/ulids"
)

// NewRow is connect to database
func NewRowUID() (*jsons.Object, error) {
	myulid, err := ulids.New()
	if err != nil {
		return nil, err
	}
	return NewRow(myulid.String()), nil
}
