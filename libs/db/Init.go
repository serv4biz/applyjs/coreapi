package db

import (
	"gitlab.com/serv4biz/letsgo"
)

func Init() error {
	jsoConfigDB, err := letsgo.GetConfig("database")
	if err != nil {
		return err
	}

	DBType = jsoConfigDB.String("txt_type")
	DBHost = jsoConfigDB.String("txt_host")
	DBPort = jsoConfigDB.Int("int_port")
	DBUser = jsoConfigDB.String("txt_username")
	DBPass = jsoConfigDB.String("txt_password")
	DBName = jsoConfigDB.String("txt_dbname")
	return nil
}
