package db

import (
	"time"

	"gitlab.com/serv4biz/gfp/jsons"
)

// NewRow is connect to database
func NewRow(txtUID string) *jsons.Object {
	jsoItem := jsons.ObjectNew(0)
	jsoItem.PutString("txt_uid", txtUID)
	jsoItem.PutInt("int_stamp", int(time.Now().UTC().Unix()))
	return jsoItem
}
