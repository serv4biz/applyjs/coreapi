package db

import (
	"gitlab.com/serv4biz/gfp/dbs"
)

// Connect is connect to database
func Connect() (*dbs.DB, error) {
	return dbs.Connect(dbs.DriverPostgreSQL, DBHost, DBPort, DBUser, DBPass, DBName)
}
