package telegram

import (
	"io"
	"net/http"
	"net/url"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func SendMessage(userId string, message string) (*jsons.Object, error) {
	jsoConfig, err := letsgo.GetConfig("telegram")
	if err != nil {
		return nil, err
	}

	val := url.Values{}
	val.Set("chat_id", userId)
	val.Set("text", message)

	txtURL := "https://api.telegram.org/bot" + jsoConfig.String("txt_access_token") + "/sendMessage?" + val.Encode()
	method := "POST"

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return jsons.ObjectFromByte(body)
}
