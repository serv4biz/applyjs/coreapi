package telegram

import (
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func GetFile(fileId string) (*jsons.Object, error) {
	jsoConfig, err := letsgo.GetConfig("telegram")
	if err != nil {
		return nil, err
	}

	txtURL := "https://api.telegram.org/bot" + jsoConfig.String("txt_access_token") + "/getFile"
	method := "POST"

	jsoParams := jsons.ObjectNew(0)
	jsoParams.PutString("file_id", fileId)
	param, err := jsoParams.ToString()
	if err != nil {
		return nil, err
	}
	payload := strings.NewReader(param)

	client := &http.Client{}
	req, err := http.NewRequest(method, txtURL, payload)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	return jsons.ObjectFromByte(body)
}
