module coreapi

go 1.24

replace coreapi => ./

require (
	gitlab.com/serv4biz/gfp v1.2.6
	gitlab.com/serv4biz/letsgo v1.4.3
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.10.0 // indirect
)

require (
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/sendgrid/rest v2.6.9+incompatible
	github.com/sendgrid/sendgrid-go v3.16.0+incompatible
	golang.org/x/net v0.35.0 // indirect
)
