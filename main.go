package main

import (
	"coreapi/libs/db"
	"coreapi/libs/telegram"
	"coreapi/workers"
	"fmt"
	"time"

	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func main() {
	fmt.Println("CoreAPI Running")
	err := db.Init()
	logs.Panic(err, true)
	logs.Panic(LoadAppInfo(), true)
	logs.Panic(LoadAPIKey(), true)

	// Telegram
	jsoConfigTel, err := letsgo.GetConfig("telegram")
	logs.Panic(err, true)
	_, err = telegram.SetWebhook(jsoConfigTel.String("txt_webhook"))
	logs.Panic(err, true)

	go func() {
		for {
			err := workers.OTPExpired()
			logs.Error(err, true)
			<-time.After(time.Minute)
		}
	}()

	go func() {
		for {
			err := workers.ClearSession()
			logs.Error(err, true)
			<-time.After(time.Second * time.Duration(letsgo.MaxSession))
		}
	}()

	LetsGo()
	LetsGoAPI()
	LetsGoModule()

	/*jsoData := jsons.ObjectNew(0)
	jsoData.PutString("redirect", "https://www.youtube.com")
	jsoData.PutString("scope", "https://trinity-webapp.s4blab.com")

	jsoOp := jsons.ObjectNew(0)
	jsoOp.PutString("body", "สวัสดี")
	jsoOp.PutObject("data", jsoData)
	webpush.SendNotification("49f74b9b2bb6b2531ddf126a7792dc591fd81b367939b3106406987259062450", "ApplyJS", jsoOp)*/

	letsgo.Listen(0)
}
