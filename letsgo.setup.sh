#!/bin/bash

mkdir tmp

mkdir apis
mkdir configs
mkdir languages
mkdir modules
mkdir sockets
mkdir templates
mkdir sessions

git clone https://gitlab.com/serv4biz/letsgo.git ./tmp/letsgo
cp -rf ./tmp/letsgo/apps/setups/* ./

cp -rf ./tmp/letsgo/apps/apis/Ping.go ./apis/Ping.go
cp -rf ./tmp/letsgo/apps/modules/Index.go ./modules/Index.go
cp -rf ./tmp/letsgo/apps/sockets/Echo.go ./sockets/Echo.go
cp -rf ./tmp/letsgo/apps/templates/index.html ./templates/index.html

cp -rf ./tmp/letsgo/apps/letsgo.$1.$2 ./letsgo.$1.$2
cp -rf ./tmp/letsgo/apps/configs/letsgo.compile.json ./configs/letsgo.compile.json

cp -rf ./tmp/letsgo/apps/letsgo.$1.$2 ./letsgo.$1.$2
cp -rf ./tmp/letsgo/apps/configs/letsgo.compile.json ./configs/letsgo.compile.json
cp -rf ./tmp/letsgo/apps/configs/letsgo.json ./configs/letsgo.json
cp -rf ./tmp/letsgo/apps/configs/mimetype.json ./configs/mimetype.json

chmod -R 755 letsgo.$1.$2
chmod -R 755 build.sh
chmod -R 755 run.sh

go get gitlab.com/serv4biz/letsgo
go get gitlab.com/serv4biz/gfp

rm -rf tmp