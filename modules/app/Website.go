package app

import (
	"coreapi/libs/app"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Website(c *letsgo.Context, p *jsons.Object) string {
	return app.JSOInfo.String("txt_website")
}
