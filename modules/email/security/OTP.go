package security

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func OTP(c *letsgo.Context, p *jsons.Object) string {
	return c.Template("email/security/otp", p)
}
