package user

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Passwd_Reset(c *letsgo.Context, p *jsons.Object) string {
	return c.Template("email/user/passwd_reset", p)
}
