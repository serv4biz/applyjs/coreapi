package emails

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Header(c *letsgo.Context, p *jsons.Object) string {
	return c.Template("email/header", p)
}
