package emails

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Style(c *letsgo.Context, p *jsons.Object) string {
	return c.Template("email/style", p)
}
