#!/bin/bash

mkdir tmp

git clone https://gitlab.com/serv4biz/letsgo.git ./tmp/letsgo
cp -rf ./tmp/letsgo/apps/letsgo.$1.$2 ./letsgo.$1.$2
cp -rf ./tmp/letsgo/apps/configs/letsgo.compile.json ./configs/letsgo.compile.json.txt
cp -rf ./tmp/letsgo/apps/configs/letsgo.json ./configs/letsgo.json.txt
cp -rf ./tmp/letsgo/apps/configs/mimetype.json ./configs/mimetype.json

chmod -R 755 letsgo.$1.$2
chmod -R 755 build.sh
chmod -R 755 run.sh
rm -rf tmp