package main

import (
	"coreapi/libs/app"
	"coreapi/libs/db"

	"gitlab.com/serv4biz/gfp/jsons"
)

func LoadAPIKey() error {
	dbConn, err := db.Connect()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	jsaList, err := dbConn.SelectRow("public.api", "*", "txt_uid != '#'", "", 0, -1)
	if err != nil {
		return err
	}

	err = jsaList.EachObject(func(index int, value *jsons.Object) error {
		item := new(app.APIKey)
		item.Key = value.String("txt_uid")
		app.MapAPIKey[item.Key] = item
		return nil
	})
	return err
}
