package main

import (
	"coreapi/libs/app"

	"gitlab.com/serv4biz/letsgo"
)

func LoadAppInfo() error {
	var err error = nil
	app.JSOInfo, err = letsgo.GetConfig("appinfo")
	if err != nil {
		return err
	}
	return nil
}
