#!/bin/bash

RUNLETSGO="letsgo.$1.$2"
RUNNAME="app.$1.$2"
PORT=$3

# update letso
rm -rf ./$RUNLETSGO
cp ~/serv4biz/letsgo/apps/$RUNLETSGO ./$RUNLETSGO
chmod -R 755 $RUNLETSGO

clear
./$RUNLETSGO
./build.sh

kill -9 $(lsof -t -i:$PORT)

chmod -R 755 $RUNNAME
./$RUNNAME $4