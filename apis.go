package main

import (
	apis "coreapi/apis"
	apis_v1_contact "coreapi/apis/v1/contact"
	apis_v1_email "coreapi/apis/v1/email"
	apis_v1_geoip "coreapi/apis/v1/geoip"
	apis_v1_phone "coreapi/apis/v1/phone"
	apis_v1_session "coreapi/apis/v1/session"
	apis_v1_storage "coreapi/apis/v1/storage"
	apis_v1_telegram "coreapi/apis/v1/telegram"
	apis_v1_user "coreapi/apis/v1/user"
	apis_v1_webpush "coreapi/apis/v1/webpush"
	"gitlab.com/serv4biz/letsgo"
)

func LetsGoAPI() {
	letsgo.AddAPIFunc("delete", apis.Delete)
	letsgo.AddAPIFunc("fetch", apis.Fetch)
	letsgo.AddAPIFunc("insert", apis.Insert)
	letsgo.AddAPIFunc("ping", apis.Ping)
	letsgo.AddAPIFunc("query", apis.Query)
	letsgo.AddAPIFunc("ulid", apis.ULID)
	letsgo.AddAPIFunc("update", apis.Update)
	letsgo.AddAPIFunc("v1/contact/add", apis_v1_contact.Add)
	letsgo.AddAPIFunc("v1/contact/count", apis_v1_contact.Count)
	letsgo.AddAPIFunc("v1/contact/delete", apis_v1_contact.Delete)
	letsgo.AddAPIFunc("v1/contact/update", apis_v1_contact.Update)
	letsgo.AddAPIFunc("v1/email/add", apis_v1_email.Add)
	letsgo.AddAPIFunc("v1/email/delete", apis_v1_email.Delete)
	letsgo.AddAPIFunc("v1/email/hint", apis_v1_email.Hint)
	letsgo.AddAPIFunc("v1/email/otp_check", apis_v1_email.OTP_Check)
	letsgo.AddAPIFunc("v1/email/otp_send", apis_v1_email.OTP_Send)
	letsgo.AddAPIFunc("v1/geoip/add", apis_v1_geoip.Add)
	letsgo.AddAPIFunc("v1/geoip/delete", apis_v1_geoip.Delete)
	letsgo.AddAPIFunc("v1/geoip/load", apis_v1_geoip.Load)
	letsgo.AddAPIFunc("v1/phone/add", apis_v1_phone.Add)
	letsgo.AddAPIFunc("v1/phone/delete", apis_v1_phone.Delete)
	letsgo.AddAPIFunc("v1/phone/hint", apis_v1_phone.Hint)
	letsgo.AddAPIFunc("v1/phone/otp_check", apis_v1_phone.OTP_Check)
	letsgo.AddAPIFunc("v1/phone/otp_send", apis_v1_phone.OTP_Send)
	letsgo.AddAPIFunc("v1/session/delete", apis_v1_session.Delete)
	letsgo.AddAPIFunc("v1/session/load", apis_v1_session.Load)
	letsgo.AddAPIFunc("v1/session/put", apis_v1_session.Put)
	letsgo.AddAPIFunc("v1/session/put_data", apis_v1_session.Put_Data)
	letsgo.AddAPIFunc("v1/session/put_geoip", apis_v1_session.Put_GeoIp)
	letsgo.AddAPIFunc("v1/session/put_user", apis_v1_session.Put_User)
	letsgo.AddAPIFunc("v1/storage/delete", apis_v1_storage.Delete)
	letsgo.AddAPIFunc("v1/storage/insert", apis_v1_storage.Insert)
	letsgo.AddAPIFunc("v1/telegram/botinfo", apis_v1_telegram.BotInfo)
	letsgo.AddAPIFunc("v1/telegram/message", apis_v1_telegram.Message)
	letsgo.AddAPIFunc("v1/telegram/otp_check", apis_v1_telegram.OTP_Check)
	letsgo.AddAPIFunc("v1/telegram/otp_connect", apis_v1_telegram.OTP_Connect)
	letsgo.AddAPIFunc("v1/telegram/otp_send", apis_v1_telegram.OTP_Send)
	letsgo.AddAPIFunc("v1/telegram/stream", apis_v1_telegram.Stream)
	letsgo.AddAPIFunc("v1/user/passwd_reset", apis_v1_user.Passwd_Reset)
	letsgo.AddAPIFunc("v1/user/register", apis_v1_user.Register)
	letsgo.AddAPIFunc("v1/webpush/config", apis_v1_webpush.Config)
	letsgo.AddAPIFunc("v1/webpush/delete", apis_v1_webpush.Delete)
	letsgo.AddAPIFunc("v1/webpush/insert", apis_v1_webpush.Insert)
	letsgo.AddAPIFunc("v1/webpush/load", apis_v1_webpush.Load)

}
