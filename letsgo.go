package main

import (
	"gitlab.com/serv4biz/letsgo"
)

func LetsGo() {
	letsgo.APP_MODULE = "coreapi"

	letsgo.API_DIR = "apis"
	letsgo.API_ROUTE = "/"

	letsgo.SOCKET_DIR = "sockets"
	letsgo.SOCKET_ROUTE = "/socket"

	letsgo.MODULE_DIR = "modules"
	letsgo.LANGUAGE_DIR = "languages"
	letsgo.TEMPLATE_DIR = "templates"
	letsgo.SESSION_DIR = "sessions"
}
