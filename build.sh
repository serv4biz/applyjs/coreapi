#!/bin/bash

declare -A platforms=(
    ["linux_amd64"]="app.linux.amd64"
    ["linux_arm64"]="app.linux.arm64"
    ["darwin_amd64"]="app.darwin.amd64"
    ["darwin_arm64"]="app.darwin.arm64"
    ["windows_amd64"]="app.windows.amd64"
    ["windows_arm64"]="app.windows.arm64"
    ["freebsd_amd64"]="app.freebsd.amd64"
    ["freebsd_arm64"]="app.freebsd.arm64"
)

export GO111MODULE=auto

for platform in "${!platforms[@]}"; do
    IFS="_" read -r GOOS GOARCH <<< "$platform"
    output_name="${platforms[$platform]}"
    
    rm -f "$output_name"
    export GOOS GOARCH
    go build -o "$output_name" -ldflags="-s -w"
    
    if [ "$GOOS" = "linux" ]; then
        if [ "$GOARCH" = "arm64" ]; then
            if command -v aarch64-linux-gnu-strip &> /dev/null; then
                aarch64-linux-gnu-strip -s "$output_name"
            else
                echo "aarch64-linux-gnu-strip not found, skipping strip for $output_name"
            fi
        else
            if command -v llvm-strip &> /dev/null; then
                llvm-strip -s "$output_name"
            else
                strip -s "$output_name"
            fi
        fi
    fi
done